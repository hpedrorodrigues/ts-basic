'use strict';

/**
 * @constructor
 */
function ActionUtil() {
}

ActionUtil.prototype.isNumber = function (value) {
    return !isNaN(parseFloat(value));
};

ActionUtil.prototype.getTimeStamp = function () {
    return (window.performance.timing.navigationStart + window.performance.now());
};