'use strict';

_.mixin({
    'notWhere': function (obj, attrs) {
        return _.filter(obj, _.negate(_.matches(attrs)));
    }
});