'use strict';

function logActionsTable(actions) {

    var separator = '-------------'
        , separatorUnderscore = '____________'
        , breakLine = '\n'
        , uniqueActions = _
        .uniq(actions, function (action) {
            return action.data;
        })
        , onlyValidUniqueActions = _.notWhere(uniqueActions, {operation: Constants.OPERATION.COMMIT})
        , tableText = _
        .reduce(onlyValidUniqueActions, function (text, action) {
            return text + ('|_' + action.data + '_|_' + action.readN + '_|_' + action.writeN + '_|') + breakLine
        }, breakLine);

    console.log(
        breakLine
        + separatorUnderscore
        + breakLine
        + '|_D_|_R_|_W_|'
        + breakLine
        + separator
        + tableText
        + separator
    );
}

window.addEventListener('load', function () {

    var buttonTest = document.getElementById('btn-test')
        , inputActions = document.getElementById('input-actions')
        , actionManager = new ActionManager()
        , domManager = new DOMManager()
        , validator = new Validator();

    Constants.SEPARATOR.SUPPORTED.forEach(function (id) {
        var element = document.getElementById(id);

        element.addEventListener('click', function (event) {
            event.preventDefault();
            actionManager.changeSeparator(domManager.checkSeparator(id));
        });
    });

    domManager.infoAlert('Sem transações para validar :)');

    buttonTest.addEventListener('click', function () {

        var actions = actionManager.extract(inputActions.value)
            , errors = validator.validateActions(actions);

        if (actions && typeof actions === 'object') {
            if (errors && errors.length) {

                console.log('Errors', errors);

                var error = _.reduce(errors, function (text, error) {
                    return text + (error.operation + error.number + '(' + error.data + ')');
                }, '');

                logActionsTable(actions);

                domManager.errorAlert('Transação inválida em: ' + error);
            } else {

                domManager.successAlert('Transação válida');
            }
        } else {

            domManager.infoAlert('Sem transações para validar :)');
        }
    });
});