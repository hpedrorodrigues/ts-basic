'use strict';

/**
 * @constructor
 */
function Validator() {
}

Validator.prototype.validateAction = function (action, actions) {

    if (action.operation === Constants.OPERATION.READ) {

        if (action.readN <= action.number) {

            action.readN = action.number;

            _.each(_.filter(actions, {data: action.data}), function (foundAction) {
                foundAction.readN = action.readN;
            });
        } else {

            return action;
        }
    } else if (action.operation === Constants.OPERATION.WRITE) {

        if (action.readN <= action.number && action.writeN <= action.number) {

            action.writeN = action.number;

            _.each(_.filter(actions, {data: action.data}), function (foundAction) {
                foundAction.writeN = action.writeN;
            });
        } else {

            return action;
        }
    }
};

Validator.prototype.validateActions = function (actions) {
    if (actions) {
        return actions
            .filter(function (action) {
                return this.validateAction(action, actions);
            }.bind(this));
    } else {

        return false;
    }
};