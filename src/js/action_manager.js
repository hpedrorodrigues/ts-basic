'use strict';

/**
 * @constructor
 */
function ActionManager() {
    this.actionUtil = new ActionUtil();
    this.separator = Constants.SEPARATOR.SEMICOLON;
}

ActionManager.prototype.changeSeparator = function (separator) {
    this.separator = separator;
};

ActionManager.prototype.isValid = function (operation) {
    return operation === Constants.OPERATION.WRITE ||
        operation === Constants.OPERATION.READ ||
        operation === Constants.OPERATION.COMMIT ||
        operation === Constants.OPERATION.ABORT;
};

ActionManager.prototype.extract = function (text) {
    var self = this;

    if (text) {

        var splittedText = text.split(this.separator);

        if (splittedText.length > 1) {

            return _.map(text.split(this.separator), function (textAction) {
                var action = self.extractAction(textAction);

                if (action) {

                    action.readN = 0;
                    action.writeN = action.readN;
                } else {

                    console.log('Invalid action found extracting actions');
                }

                return action;
            });
        } else {

            return false;
        }
    } else {

        return false;
    }
};

ActionManager.prototype.extractAction = function (text) {

    if (text) {

        var util = this.actionUtil
            , operation = text.charAt(0)
            , actionInfo = (function (text) {

            var formatted = '', lastPosition = 0;

            for (var position in text) {

                var current = text[position];

                if (util.isNumber(current)) {

                    lastPosition = position;
                    formatted += current;
                } else {

                    break;
                }
            }

            return {
                number: formatted,
                lastPosition: lastPosition
            };
        }(text.slice(1)))
            , data = (function (text) {

            var matches = /\(([^)]+)\)/.exec(text);

            if (matches) {

                return matches[1] ? matches[1] : matches[0];
            } else {

                return text;
            }
        }(text.slice(actionInfo.lastPosition)));

        if (!this.isValid(operation)) {

            return false;
        }

        return {
            operation: operation,
            number: actionInfo.number,
            data: data
        }
    } else {

        return false;
    }
};