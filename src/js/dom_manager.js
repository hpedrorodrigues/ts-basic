'use strict';

/**
 * @constructor
 */
function DOMManager() {
    this.alertView = '';
}

DOMManager.prototype._createAlert = function (clazz, message) {
    if (this.alertView) {
        this.hideAlert();
    }

    var formView = document.querySelector('form');

    this.alertView = document.createElement('div');

    this.alertView.classList.add('alert', 'm-t-s', clazz);
    this.alertView.setAttribute('role', 'alert');
    this.alertView.innerHTML = message;

    formView.appendChild(this.alertView);

    return this.alertView;
};

DOMManager.prototype.infoAlert = function (message) {
    return this._createAlert('alert-info', message);
};

DOMManager.prototype.successAlert = function (message) {
    return this._createAlert('alert-success', message);
};

DOMManager.prototype.errorAlert = function (message) {
    return this._createAlert('alert-danger', message);
};

DOMManager.prototype.hideAlert = function () {
    var formView = document.querySelector('form');

    if (formView && this.alertView) {
        formView.removeChild(formView.childNodes[formView.childNodes.length - 1]);
        this.alertView = undefined;
    }
};

DOMManager.prototype.checkSeparator = function (id) {
    if (Constants.SEPARATOR.SUPPORTED.indexOf(id) != -1) {

        Constants.SEPARATOR.SUPPORTED.forEach(function (supportedId) {
            var element = document.getElementById(supportedId);
            element.classList.remove('active');
        });

        document.getElementById(id).classList.add('active');

        return Constants.SEPARATOR[id.toUpperCase()];
    }
};