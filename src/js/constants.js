'use strict';

var Constants = {
    OPERATION: {
        WRITE: 'w',
        READ: 'r',
        COMMIT: 'c',
        ABORT: 'a'
    },
    SEPARATOR: {
        COMMA: ',',
        SEMICOLON: ';',
        SPACE: ' ',
        DOT: '.',
        SUPPORTED: ['semicolon', 'comma', 'dot', 'space']
    }
};